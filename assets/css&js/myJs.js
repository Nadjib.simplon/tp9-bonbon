let verGauche = 45
let verBas = 50
let verGaucheBase = 13.6
let verBasBase = 10
let timer = 0;
let avionHeight = 2;
let avionWidth = 1;
let positionXCible1 = 10
let positionYCible1 = 20
let positionXCible2 = 70
let positionYCible2 = 60
let positionXCible3 = 35
let positionYCible3 = 30
let start = false
let tailleCible1 = 30
let tailleCible2 = 30
let countdown = 65
let recharge = 2
let resetVie = 0;
let vieCible1 = 2
let vieCible2 = 3
let vieCible3 = 5
let cible1Visible = false
let cible2Visible = false
let cible3Visible = false

$('#cible3').hide()
$('#cible2').hide()
$('#cible1').hide()
$('#vie').hide()
$('#vieBoss').hide()

$('#avion').hide()
$('#title').css('opacity', '1')


$('.mybtn').click(function(){
    resetAll()

})

resetAll = () =>{
    timer = 0
    start=true
    resetVie1()
    resetVie2()
    resetVie3()
    $('#title').css('opacity','0')
    $('#viseur').show()
    $('#avion').show()
    $('#viseur').fadeIn()



}

// SON



// SON TIR
let objReload = document.createElement("audio");
objReload.src = "./assets/sons/reload.mp3";
objReload.volume = 0.1;
objReload.autoPlay = false;
objReload.preLoad = true;
objReload.controls = true;

let objCol = document.createElement("audio");
objCol.src = "./assets/sons/col.mp3";
objCol.volume = 0.1;
objCol.autoPlay = false;
objCol.preLoad = true;
objCol.controls = true;


let obj = document.createElement("audio");
obj.src = "./assets/sons/tir.mp3";
obj.volume = 0.1;
obj.autoPlay = false;
obj.preLoad = true;
obj.controls = true;

$(".playSound").click(function() {

    // ;
});

// SON TIR FIN
var audioElement = document.createElement('audio');
audioElement.setAttribute('src', './assets/sons/musique.mp3');

audioElement.addEventListener('ended', function() {
    this.play();
}, false);

audioElement.addEventListener("canplay",function(){
    $("#length").text("Duration:" + audioElement.duration + " seconds");
    $("#source").text("Source:" + audioElement.src);
    $("#status").text("Status: Ready to play").css("color","green");
});

audioElement.addEventListener("timeupdate",function(){
    $("#currentTime").text("Current second:" + audioElement.currentTime);
});

$('#play').click(function() {
    audioElement.play();
    $("#status").text("Status: Playing");
});

$('#pause').click(function() {
    audioElement.pause();
    $("#status").text("Status: Paused");
});

$('#restart').click(function() {
    audioElement.currentTime = 0;
});




// Fin son





nbRandom = () =>  {
    return Math.floor(Math.random() * Math.floor(70));
}
setInterval(function(){
    countdown--;
    recharge++ ;
    $('#munitions').val(recharge);
    timer++ ;
    $('#time').html(`<p class="uk-text-default">Limite de temp : ${countdown}</p>`)


    if (recharge == 3){
        recharge =3
    }
    if(start == false) {
        timer = 0
        countdown = 66
        $('#avion').hide()

    }else{

        $('#avion').show()

    }
if (timer ==3 && start == true) {
    $('#title').hide()
    resetVie1()
    cible1()

}
    if (timer == 10 && start == true) {
        resetVie1(nbRandom,nbRandom)
        resetVie2(nbRandom,nbRandom)

        cible1()
        cible2()

    }
    if (timer ==17 && start == true) {
        resetVie1(nbRandom,nbRandom)
        resetVie2(nbRandom,nbRandom)

        cible1()
        cible2()
    }
    if (timer ==29  | timer == 42 && start == true) {

        resetVie1(nbRandom,nbRandom)
        resetVie2(nbRandom,nbRandom)

        cible1()
        cible2()
    }
    if (timer ==50 && start == true) {
        $('#text').text('Attention BOSS !')
        $('#title').show()
        $('.mybtn').hide()
        $('#title').css('opacity','1')
        resetVie3()
        cible3()


        if (timer ==52 && start == true) {
            $('#title').hide()

        }
    }
    if (timer ==65 && start == true) {


        timer=0
        start=false
        $('#text').text('Temps Ecoulé !')
        $('#title').show()
        $('#viseur').fadeOut()
        $('#title').css('opacity', '1')


    }





}, 1000);

window.addEventListener('keyup', function(event) {
    switch (event.key) {
        case 'ArrowLeft':

            $('.scene').removeClass('tourneAGauche')
            break;
        case 'ArrowRight':
            $('.scene').removeClass('tourneADroite')

            break;
        case 'ArrowUp':

            $('.scene').removeClass('monte')
            break;
        case 'ArrowDown':
            $('.scene').removeClass('descend')

            break;
        case 'Shift':


            break;

        }})



window.addEventListener('keydown', function(event) {
    switch (event.key) {
        case 'ArrowLeft':
            verGauche = verGauche - 1;
            document.documentElement.style.setProperty('--left', verGauche+'vw');
            document.documentElement.style.setProperty('--leftBase', verGaucheBase+'vw');

            $('.scene').addClass('tourneAGauche')
            break;
        case 'ArrowRight':
            verGauche = verGauche + 1;
            document.documentElement.style.setProperty('--left', verGauche+'vw');
            document.documentElement.style.setProperty('--leftBase', verGaucheBase+'vw');

            $('.scene').addClass('tourneADroite')

            break;
        case 'ArrowUp':
            verBas = verBas + 1;
            document.documentElement.style.setProperty('--bottom', verBas+'vh');
            document.documentElement.style.setProperty('--bottomBase', verBasBase+'vh');

            $('.scene').addClass('monte')

            break;
        case 'ArrowDown':
            verBas = verBas - 1;
            document.documentElement.style.setProperty('--bottom', verBas+'vh');
            document.documentElement.style.setProperty('--bottomBase', verBasBase+'vh');

            $('.scene').addClass('descend')

            break;

        case 'Shift':



            if (recharge >= 2 ){
                recharge = 0
            $('#munitions').val(recharge)
            $('#tir').addClass("tirer");
            setTimeout(function(){ $('#tir').removeClass("tirer"); }, 500);

            obj.play()





            if (verGauche < positionXCible1 + tailleCible1 &&
                verGauche + avionWidth > positionXCible1 &&
                verBas < positionYCible1 + tailleCible1 &&
                avionHeight + verBas > positionYCible1 &&
                cible1Visible === true) {
                $('#vie').val(vieCible1)
                $('#label').text('Marie')
                $('#vie').show()
                $('#label').show()
                objCol.play()
                vieCible1--
                $('#vie').val(vieCible1)

                if (vieCible1 < 1){
                    $('#cible1').hide()
                    $('#vie').hide()
                    $('#label').hide()


                }
            }
            if (verGauche < positionXCible2 + tailleCible1 &&
                verGauche + avionWidth > positionXCible2 &&
                verBas < positionYCible2 + tailleCible1 &&
                avionHeight + verBas > positionYCible2 &&
                cible2Visible === true) {
                objCol.play()

                $('#vie').val(vieCible2)
                $('#label').text('Cedric')
                $('#label').show()
                $('#vie').show()
                vieCible2--
                $('#vie').val(vieCible2)
                if (vieCible2 < 1){
                    $('#cible2').hide()
                    $('#vie').hide()
                    $('#label').hide()



                }
            }
            if (verGauche < positionXCible3 + tailleCible1 &&
                verGauche + avionWidth > positionXCible3 &&
                verBas < positionYCible3 + tailleCible1 &&
                avionHeight + verBas > positionYCible3 &&
                cible3Visible === true) {
                objCol.play()


                $('#vieBoss').val(vieCible3)
                $('#label').text('Thomas')
                $('#vieBoss').show()
                $('#label').show()


                vieCible3--
                $('#vieBoss').val(vieCible3)


                if (vieCible3 < 1){
                    $('#cible3').hide()
                    $('#vieBoss').hide()
                    $('#label').hide()
                    $('#text').text(' Boss Thomas à été vaincu !')
                    $('#title').show()
                    $('#viseur').fadeOut()
                    $('#title').css('opacity', '1')

                }
            }
            }else{
                objReload.play()
            }
            break;
    }})

resetVie1 = (y,x) => {
    cible1Visible = false
    vieCible1 = 2
    document.documentElement.style.setProperty('--bottomCible1',y+'vh')
    document.documentElement.style.setProperty('--leftCible1',x+'vw')
    document.documentElement.style.setProperty('--cible1Width',1+'vw')
    document.documentElement.style.setProperty('--cible1Height',1+'vh')
    $('#cible1').hide()
}
cible1 = () => {
    $('#cible1').show()
    cible1Visible = true

    document.documentElement.style.setProperty('--bottomCible1',positionYCible1+'vh')
    document.documentElement.style.setProperty('--leftCible1',positionXCible1+'vw')
    document.documentElement.style.setProperty('--cible1Width',tailleCible1+'vw')
    document.documentElement.style.setProperty('--cible1Height',tailleCible1+'vh')

}

resetVie2 = (y,x) => {

    vieCible2 = 3
    document.documentElement.style.setProperty('--bottomCible2',y+'vh')
    document.documentElement.style.setProperty('--leftCible2',x+'vw')
    document.documentElement.style.setProperty('--cible2Width',1+'vw')
    document.documentElement.style.setProperty('--cible2Height',1+'vh')
    cible2Visible = false
    $('#cible1').hide()
}


cible2 = () => {
    $('#cible2').show()
    cible2Visible = true

    document.documentElement.style.setProperty('--bottomCible2',positionYCible2+'vh')
    document.documentElement.style.setProperty('--leftCible2',positionXCible2+'vw')
    document.documentElement.style.setProperty('--cible2Width',tailleCible2+'vw')
    document.documentElement.style.setProperty('--cible2Height',tailleCible2+'vh')

}

resetVie3 = () => {
    $('#cible3').hide()
    cible3Visible = false

    vieCible3 = 5
    document.documentElement.style.setProperty('--bottomCible3',45+'vh')
    document.documentElement.style.setProperty('--leftCible3',45+'vw')
    document.documentElement.style.setProperty('--cible3Width',1+'vw')
    document.documentElement.style.setProperty('--cible3Height',1+'vh')
}


cible3 = () => {
    $('#cible3').show()
    cible3Visible = true

    document.documentElement.style.setProperty('--bottomCible3',positionYCible3+'vh')
    document.documentElement.style.setProperty('--leftCible3',positionXCible3+'vw')
    document.documentElement.style.setProperty('--cible3Width',tailleCible1+'vw')
    document.documentElement.style.setProperty('--cible3Height',tailleCible1+'vh')

}





